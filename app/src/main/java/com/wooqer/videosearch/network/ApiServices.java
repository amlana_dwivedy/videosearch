package com.wooqer.videosearch.network;


import com.wooqer.videosearch.models.VimeoSearchVo;
import com.wooqer.videosearch.models.YoutubeSearchVo;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */
public interface ApiServices {

    @GET(EndPointsConfig.YOUTUBE_SEARCH)
    void searchInYoutube(@Query("key") String key, @Query("q") String q, @Query("maxResults") int maxResults,
                         @Query("pageToken") String nextPageToken, Callback<YoutubeSearchVo> callback);

    @GET(EndPointsConfig.VIMEO_SEARCH)
    void serchInVimeo(@Query("access_token") String access_token, @Query("query") String query,
                      @Query("page") int page, Callback<VimeoSearchVo> callback);

}