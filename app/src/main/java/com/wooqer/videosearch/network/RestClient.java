package com.wooqer.videosearch.network;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */
public class RestClient {
    private ApiServices mApiServices;

    OkHttpClient okHttpClient = new OkHttpClient();

    public RestClient(boolean isYoutube) {
        RestAdapter restAdapter;
        if (isYoutube)
            restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint("https://www.googleapis.com/youtube/v3")
                    .setClient(new OkClient(okHttpClient))
                    .build();
        else
            restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint("https://api.vimeo.com")
                    .setClient(new OkClient(okHttpClient))
                    .build();

        mApiServices = restAdapter.create(ApiServices.class);

    }

    public ApiServices getApiServices() {
        return mApiServices;
    }


}
