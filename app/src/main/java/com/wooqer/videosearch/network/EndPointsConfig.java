package com.wooqer.videosearch.network;

/**
 * Created by Venkatesh Devale on 24-08-2015.
 */
public interface EndPointsConfig {

    String YOUTUBE_SEARCH = "/search?part=snippet&order=viewCount&type=video+&videoDefinition=high";
    String VIMEO_SEARCH = "/videos";

}
