package com.wooqer.videosearch.utils;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.text.TextUtils;

/**
 * Created by Venkatesh Devale on 24-08-2015.
 */

public class DialogUtil {

    public static ProgressDialog showDialog(Context context, String message) {

        final ProgressDialog dialog = new ProgressDialog(context);
        if (!TextUtils.isEmpty(message))
            dialog.setTitle(message);
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return dialog;

    }

    public static void showAlert(Context context, String message) {

        new AlertDialog.Builder(context)
                .setTitle("LATCH")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                }).show();

    }

}
