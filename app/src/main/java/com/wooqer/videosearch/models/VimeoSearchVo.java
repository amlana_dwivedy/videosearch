package com.wooqer.videosearch.models;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */
public class VimeoSearchVo {

    public int total;
    public int page;
    public int per_page;
    public Data[] data;

    public class Data{
        public String uri;
        public String name;
        public String link;
        public Pictures pictures;

    }

    public class Pictures{
        public Sizes[] sizes;
    }

    public class Sizes{
        public int width;
        public int height;
        public String link;
    }
}
