package com.wooqer.videosearch;

import android.content.Context;

import com.orm.SugarApp;

/**
 * Created by Venkatesh Devale on 24/8/15.
 */
public class MyApplication extends SugarApp {

    private Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = this;
    }

}
