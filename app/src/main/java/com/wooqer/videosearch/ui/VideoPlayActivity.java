package com.wooqer.videosearch.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.wooqer.videosearch.R;
import com.wooqer.videosearch.utils.DialogUtil;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */

public class VideoPlayActivity extends Activity {

    private Context mContext;
    private WebView mWebView;
    private FrameLayout mCustomViewContainer;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private View mCustomView;
    private myWebChromeClient mWebChromeClient;
    private myWebViewClient mWebViewClient;
    private Dialog mLoadingDialog;

    private final String YOUTUBE = "http://www.youtube.com/embed/";
    private final String VIMEO = "https://player.vimeo.com/video/";

    private String videoId;
    private boolean isYoutube;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_play_layout);

        if (getIntent().getExtras() != null) {
            videoId = getIntent().getExtras().getString("videoId");
            isYoutube = getIntent().getExtras().getBoolean("isYoutube");
        }

        mContext = this;
        mCustomViewContainer = (FrameLayout) findViewById(R.id.customViewContainer);
        mWebView = (WebView) findViewById(R.id.webView);

        mWebViewClient = new myWebViewClient();
        mWebView.setWebViewClient(mWebViewClient);

        mWebChromeClient = new myWebChromeClient();
        mWebView.setWebChromeClient(mWebChromeClient);
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.getSettings().setAppCacheEnabled(true);
        mWebView.getSettings().setBuiltInZoomControls(false);
        mWebView.getSettings().setSaveFormData(true);
        if (isYoutube)
            mWebView.loadUrl(YOUTUBE + videoId);
        else
            mWebView.loadUrl(VIMEO + videoId);

    }

    private boolean inCustomView() {
        return (mCustomView != null);
    }

    private void hideCustomView() {
        mWebChromeClient.onHideCustomView();
    }

    @Override
    protected void onPause() {
        super.onPause();
        mWebView.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mWebView.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (inCustomView()) {
            hideCustomView();
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            if (inCustomView()) {
                hideCustomView();
                return true;
            }

            if ((mCustomView == null) && mWebView.canGoBack()) {
                mWebView.goBack();
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.anim_slide_in_from_left, R.anim.anim_alpha_fade_out_slide_out_right);

    }

    class myWebChromeClient extends WebChromeClient {
        private Bitmap mDefaultVideoPoster;
        private View mVideoProgressView;

        /*This method was deprecated in API level 18. But for compatibility, have added this one which redirects to the updated method*/
        @Override
        public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
            onShowCustomView(view, callback);
        }

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {

            // if a view already exists then immediately terminate the new one
            if (mCustomView != null) {
                callback.onCustomViewHidden();
                return;
            }
            mCustomView = view;
            mWebView.setVisibility(View.GONE);
            mCustomViewContainer.setVisibility(View.VISIBLE);
            mCustomViewContainer.addView(view);
            mCustomViewCallback = callback;
        }

        @Override
        public View getVideoLoadingProgressView() {

            if (mVideoProgressView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                mVideoProgressView = inflater.inflate(R.layout.video_progress, null);
            }
            return mVideoProgressView;
        }

        @Override
        public void onHideCustomView() {
            super.onHideCustomView();
            if (mCustomView == null)
                return;

            mWebView.setVisibility(View.VISIBLE);
            mCustomViewContainer.setVisibility(View.GONE);

            // Hide the custom view.
            mCustomView.setVisibility(View.GONE);

            // Remove the custom view from its container.
            mCustomViewContainer.removeView(mCustomView);
            mCustomViewCallback.onCustomViewHidden();

            mCustomView = null;
        }
    }

    class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            return super.shouldOverrideUrlLoading(view, url);
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap facIcon) {
            DialogUtil dialogUtil = new DialogUtil();
            mLoadingDialog = dialogUtil.showDialog(mContext, getString(R.string.loading_video));
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mLoadingDialog.dismiss();
        }

    }

}
