package com.wooqer.videosearch.ui;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.wooqer.videosearch.R;
import com.wooqer.videosearch.adapters.SearchResultsAdapter;
import com.wooqer.videosearch.manager.VideoSearchManager;
import com.wooqer.videosearch.utils.NetworkManager;

public class MainActivity extends Activity implements NetworkManager.NetworkStatusChangeListener {

    private Context mContext;
    private AutoCompleteTextView mSearchEt;
    private ListView mResultsLv;
    private ListView mOfflineLv;
    private VideoSearchManager mVideoSearchManager;
    private NetworkManager mNetworkManager;
    private SearchResultsAdapter mResultsAdapter;

    private int mTotalItemsNumber;
    private int mItemShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        mContext = this;
        mVideoSearchManager = VideoSearchManager.newInstance();
        mNetworkManager = new NetworkManager(mContext, this);

        mSearchEt = (AutoCompleteTextView) findViewById(R.id.search_et);
        mSearchEt.setOnEditorActionListener(mSearchEtEditorActionListener);
        mSearchEt.addTextChangedListener(mTextWatcher);

        mResultsLv = (ListView) findViewById(R.id.results_lv);
        mResultsLv.setOnItemClickListener(mOnItemClickListener);
        mResultsLv.setOnScrollListener(mOnScrollListener);

        mOfflineLv = (ListView) findViewById(R.id.offline_lv);

        if (mNetworkManager.getConnectivityStatus(mContext) == NetworkManager.TYPE_NOT_CONNECTED) {
            mOfflineLv.setVisibility(View.VISIBLE);
            mResultsLv.setVisibility(View.GONE);
        }
        setOfflineAdapter();
        setOnlineAdapter();
    }

    public void setOfflineAdapter() {
        ArrayAdapter offlineAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, VideoSearchManager.mSearchedKey);
        mOfflineLv.setAdapter(offlineAdapter);
        mOfflineLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mSearchEt.setText(VideoSearchManager.mSearchedKey[position]);
                mOfflineLv.setVisibility(View.GONE);
            }
        });

    }

    public void setOnlineAdapter() {
        mResultsAdapter = new SearchResultsAdapter(mContext);
        mResultsLv.setAdapter(mResultsAdapter);
    }

    private TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if (mNetworkManager.getConnectivityStatus(mContext) == NetworkManager.TYPE_NOT_CONNECTED) {
                if (s.toString().length() == 0) {
                    mOfflineLv.setVisibility(View.VISIBLE);
                    setOfflineAdapter();
                    mResultsLv.setVisibility(View.GONE);
                }
            }
        }
    };

    private TextView.OnEditorActionListener mSearchEtEditorActionListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                mResultsLv.setVisibility(View.VISIBLE);
                mOfflineLv.setVisibility(View.GONE);

                mVideoSearchManager.searchVideos(v.getText().toString(), mNetworkManager.getConnectivityStatus(mContext), mContext);
                addAutoComplete();

                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

                return true;
            }
            return false;
        }
    };

    private AdapterView.OnItemClickListener mOnItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            if (mNetworkManager.getConnectivityStatus(mContext) != NetworkManager.TYPE_NOT_CONNECTED) {
                playVideo(mVideoSearchManager.getCommonVoList().get(position).vid,
                        mVideoSearchManager.getCommonVoList().get(position).isYoutube);
            } else {
                Toast.makeText(mContext, R.string.no_network_available, Toast.LENGTH_LONG).show();
            }
        }
    };

    private AbsListView.OnScrollListener mOnScrollListener = new AbsListView.OnScrollListener() {
        @Override
        public void onScrollStateChanged(AbsListView view, int scrollState) {
            if (mItemShown == mTotalItemsNumber) {

                if (mNetworkManager.getConnectivityStatus(mContext) != NetworkManager.TYPE_NOT_CONNECTED) {
                    Toast.makeText(mContext, R.string.loading, Toast.LENGTH_SHORT).show();
                    mVideoSearchManager.nextPageVideos(mSearchEt.getText().toString());
                } else
                    Toast.makeText(mContext, R.string.no_network_available, Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            mTotalItemsNumber = totalItemCount;
            mItemShown = firstVisibleItem + visibleItemCount;
        }
    };

    private void addAutoComplete() {
        ArrayAdapter<String> autoCompleteAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, VideoSearchManager.mSearchedKey);
        mSearchEt.setThreshold(1);
        mSearchEt.setAdapter(autoCompleteAdapter);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    private void playVideo(String videoId, boolean isYoutube) {
        Intent intent = new Intent(mContext, VideoPlayActivity.class);
        Bundle intentBundle = new Bundle();
        intentBundle.putString("videoId", videoId);
        intentBundle.putBoolean("isYoutube", isYoutube);
        intent.putExtras(intentBundle);
        startActivity(intent);
    }

    @Override
    public void networkOffline() {
        setOfflineAdapter();
    }

    @Override
    public void networkOnline() {
        setOnlineAdapter();
    }
}
