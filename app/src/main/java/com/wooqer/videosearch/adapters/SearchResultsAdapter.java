package com.wooqer.videosearch.adapters;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.drawee.view.SimpleDraweeView;
import com.wooqer.videosearch.R;
import com.wooqer.videosearch.manager.VideoSearchManager;
import com.wooqer.videosearch.models.CommonVo;

import java.util.List;

/**
 * Created by Venkatesh Devale on 23-08-2015.
 */
public class SearchResultsAdapter extends BaseAdapter implements VideoSearchManager.NotifyAdapter {

    private Context mContext;
    private VideoSearchManager mVideoSearchManager;
    private List<CommonVo> mCommonVoList;

    public SearchResultsAdapter(Context context) {
        this.mContext = context;
        Fresco.initialize(context);

        mVideoSearchManager = VideoSearchManager.newInstance();
        mVideoSearchManager.setNotifyDataInterface(this);
        mCommonVoList = mVideoSearchManager.getCommonVoList();
    }

    @Override
    public int getCount() {
        return mCommonVoList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    private class ViewHolder {
        TextView titleTv;
        SimpleDraweeView thumbnailIv;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if (convertView == null) {
            convertView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.results_lv_items_layout, parent, false);
            viewHolder = new ViewHolder();

            viewHolder.titleTv = (TextView) convertView.findViewById(R.id.title);
            viewHolder.thumbnailIv = (SimpleDraweeView) convertView.findViewById(R.id.thumbnail);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (viewHolder.titleTv != null)
            viewHolder.titleTv.setText(mCommonVoList.get(position).title);
        if (viewHolder.thumbnailIv != null) {
            viewHolder.thumbnailIv.setImageURI(Uri.parse(mCommonVoList.get(position).thumbnailUrl));
        } else
            viewHolder.thumbnailIv.setVisibility(View.GONE);

        return convertView;
    }

    @Override
    public void notifyDataSetChange() {
        mCommonVoList = mVideoSearchManager.getCommonVoList();
        notifyDataSetChanged();
    }
}
